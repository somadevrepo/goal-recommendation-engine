# Goal Recommendation Engine (GRE)

Flask app to provide goal recommendations based on user data.

## Zero 2 Hero

1. Install [docker](https://www.docker.com/get-started) and [docker-compose](https://docs.docker.com/compose/install/)
1. Build containers: `docker-compose -f docker-compose-dev.yml build`
1. Start app: `docker-compose -f docker-compose-dev.yml up`
1. (Optional): for proper debuggability:
    - Install [conda]()
    - Create the conda environment: `conda create -f environment.yml`
    - Set your project interpreter in PyCharm to the Python executable in the conda environment
    - Run the app from Pycharm

**Notes**:
- Ideally I'd like to connect the PyCharm debugger to the docker instance,
but this is only available in the professional edition of PyCharm/IntelliJ
- I have not yet figured out if it's possible to enable remote debugging
in the docker container and just expose the necessary port for PyCharm