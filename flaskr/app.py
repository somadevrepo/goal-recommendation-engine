import os

import jsonpickle
from flask import Flask, jsonify, request, Response

from flaskr.models import Query, InvalidParameters
from flaskr.recommend import make_recommendation

app = Flask(__name__)
app.config['DEBUG'] = os.environ.get('DEBUG', False)


@app.route('/recommend_goal', methods=['GET'])
def root():
    query = Query(**request.args.to_dict())
    recommendation = make_recommendation(query)
    return Response(response=jsonpickle.encode(recommendation.__getstate__()),
                    status=200,
                    mimetype="application/json")


@app.errorhandler(InvalidParameters)
def handle_invalid_value(error):
    response = jsonify(error.to_dict())
    response.status_code = 400
    return response


if __name__ == "__main__":
    app.run(host="localhost", debug=True)
