class Recommendation:

    def __init__(self, goals):
        self.goals = goals

    def __getstate__(self):
        return {"ranked_goals": self.goals}
