class Prediction:
    goal: str
    rating: float
    who5_diff: float

    def __init__(self, goal: str, rating: float, who5_diff: float):
        self.goal = goal
        self.rating = rating
        self.who5_diff = who5_diff
