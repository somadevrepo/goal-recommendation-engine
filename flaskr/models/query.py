from flaskr.models import InvalidParameters


class Query:
    gender: str
    current_goal: str
    who1: int
    who2: int
    who3: int
    who4: int
    who5: int

    def __init__(self,
                 gender: str = None,
                 current_goal: str = None,
                 who1: int = None,
                 who2: int = None,
                 who3: int = None,
                 who4: int = None,
                 who5: int = None):
        if current_goal is None or \
                gender is None or \
                who1 is None or \
                who2 is None or \
                who3 is None or \
                who4 is None or \
                who5 is None:
            raise InvalidParameters("Missing parameters", payload={
                "current_goal": current_goal,
                "gender": gender,
                "who1": who1,
                "who2": who2,
                "who3": who3,
                "who4": who4,
                "who5": who5
            })

        self.gender = gender
        self.current_goal = current_goal
        self.who1 = who1
        self.who2 = who2
        self.who3 = who3
        self.who4 = who4
        self.who5 = who5
