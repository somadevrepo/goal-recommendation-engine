import os

import numpy as np
from rpy2.robjects import r, pandas2ri

from flaskr.models import Query
from flaskr.models import Recommendation

r_libs = os.environ["R_LIB_PATH"]
r['.libPaths'](r_libs)

r.library("bnlearn")
r.library("data.table")

r('''
source("./recommend/recommend.R")
''')

recommend_r = r['recommend']


def make_recommendation(query: Query) -> Recommendation:
    predictions = recommend_r(query.gender,
                              query.current_goal,
                              query.who1,
                              query.who2,
                              query.who3,
                              query.who4,
                              query.who5)
    predictions = pandas2ri.ri2py(predictions)
    predictions["completed_goal_not_none"] = ~predictions.predicted_completed_goal.isin(["None", 'NA', np.nan, "NaN"])
    predictions["rating_not_none"] = ~predictions.predicted_rating.isin(["NaN", 'NA', np.nan])
    predictions["who5_diff_not_none"] = ~predictions.predicted_who5_diff.isin(["NaN", 'NA', np.nan])
    sorted_predictions = predictions.sort_values(by=["completed_goal_not_none",
                                                     "who5_diff_not_none",
                                                     "predicted_who5_diff",
                                                     "rating_not_none",
                                                     "predicted_rating", ],
                                                 ascending=False)
    sorted_predictions = sorted_predictions.drop("completed_goal_not_none", axis=1)
    sorted_predictions = sorted_predictions.drop("rating_not_none", axis=1)
    sorted_predictions = sorted_predictions.drop("who5_diff_not_none", axis=1)
    return Recommendation(sorted_predictions.to_dict(orient="records"))
